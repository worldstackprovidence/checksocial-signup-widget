const path = require('path');
const {merge} = require('webpack-merge');
const common = require('./webpack.common');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(common, {
  devServer: {
    contentBase: [path.join(__dirname, 'dist'), path.join(__dirname, 'test')],
    compress: true,
    port: 9000
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Production',
      template: 'index.html',
      inject: 'head'
    })
  ],
  devtool: 'inline-source-map',
  mode: 'development'
});
