import './style.scss';

const template = require("./widget.handlebars");

const EMAIL_REGEX = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+(?:[_a-z0-9-]+(\.[_a-z0-9-]+)*))?@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,20}(?:\.[a-z]{2})?)$/i

const POST_OP_NAME = 'createAccount';
const POST_QUERY = 'mutation createAccount($name: String!, $type: String!, $adminEmail: String!, $firstName: String!, $lastName: String!, $adminPhoneNumber: String) {  createAccount(name: $name, type: $type, adminEmail: $adminEmail, firstName: $firstName, lastName: $lastName, adminPhoneNumber: $adminPhoneNumber) { id __typename }}';
const POST_TYPE = 'personal'

interface FormData {
  firstName: string;
  lastName: string;
  adminEmail: string;
}

interface PostData {
  operationName: string;
  variables: {
    name: string;
    type: string;
    adminPhoneNumber: string;
  } & FormData;
  query: string;
}

const createPostData = (formData: FormData): PostData => {
  return {
    operationName: POST_OP_NAME,
    variables: {
      ...formData,
      name: formData.firstName + ' ' + formData.lastName + ' - Personal',
      type: POST_TYPE,
      adminPhoneNumber: ''
    },
    query: POST_QUERY
  }
}

interface SignupOptions {
  title?: string;
  buttonText?: string;
  supportEmail?: string;
  onSuccessfulSubmit?: (data: SignupData) => any;
}

const makeId = (length: number) => {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

/**
 * apply the handlers for the elements on the widget.
 * @param element the newly created widget
 * @param id the id of the widget
 */
const applyHandlers = (element: HTMLDivElement, id: string, opts: SignupOptions) => {
  // now add ripple effect handlers
  const buttons = element.querySelectorAll(`#${id} button.ripple-button`);
  for (const button of buttons) {
    button.addEventListener("click", createRipple);
  }
  const submitButton = element.querySelector(`#${id} button.submit-button`);
  submitButton.addEventListener("click", () => postFormData(id, opts))

  const inputs = element.querySelectorAll(`#${id} .input-section input`);
  for (const input of inputs) {
    input.addEventListener("blur", addTouched)
  }
  const emailInput = element.querySelector(`#${id} .cs-signup-email-container input`)
  emailInput.addEventListener("keyup", checkValidEmail);
  emailInput.addEventListener("paste", checkValidEmail);
  emailInput.addEventListener("blur", checkValidEmail);
}

interface SignupData {
  firstName: string;
  lastName: string;
  email: string;
}

/**
 * utility to post the data with.
 * @param url
 * @param data
 */
async function postData(url = '', data = {}) {
  // Default options are marked with *
  const response = await fetch(url, {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, *cors, same-origin
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    credentials: 'same-origin', // include, *same-origin, omit
    headers: {
      'Content-Type': 'application/json',
      'Authorization': ''
    },
    redirect: 'follow', // manual, *follow, error
    referrerPolicy: 'origin', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: JSON.stringify(data) // body data type must match "Content-Type" header
  });
  return response.json(); // parses JSON response into native JavaScript objects
}

/**
 * check if the response received from the signup post was successful.
 */
const isSuccessResponse = (json: any): boolean => {
  return !!json && !!json.data && !!json.data.createAccount
}

/**
 * uses a regular expression to validate the given email, true if valid, false otherwise.
 * @param email
 */
const verifyValidEmail = (email: string): boolean => !!email && !!EMAIL_REGEX.exec(email);

let removeErrorId: any;

function displayError(id: string, errorMessage: string) {
  if (!!removeErrorId) {
    clearTimeout(removeErrorId);
  }
  const element = document.querySelector(`#${id} .error-message`);
  element.innerHTML = `<p>${errorMessage}</p>`
  const container = document.querySelector(`#${id}`);
  container.classList.add('error');
  removeErrorId = setTimeout(() => {
    container.classList.remove('error');
  }, 10000);
}

/**
 * function called when the submit button is pressed.
 */
const postFormData = async (id: string, opts: SignupOptions) => {
  const form = document.querySelector(`#${id} form.cs-signup-widget-form`) as HTMLFormElement;
  if (!form.checkValidity()) {
    for (const input of form.querySelectorAll('.input-section input')) {
      input.classList.add('touched');
    }
    displayError(id, '* First name, last name, and valid email address are required');
    console.warn('First name, last name, and valid email address are required');
  } else {
    // remove error messages
    const inputs = Array.from(form.querySelectorAll('.input-section input'));
    const rawFormData = formToJSON(inputs);
    const formData: FormData = {
      firstName: rawFormData['cs-signup-firstname'],
      lastName: rawFormData['cs-signup-lastname'],
      adminEmail: rawFormData['cs-signup-email']
    }
    const pData: PostData = createPostData(formData);
    console.debug('data will be sent', pData);
    // let data = createPostData();
    toggleFormDisabled(true);
    toggleFormSubmitting(id, true);
    try {
      let json = await postData('https://api.checksocial.com.au/graphql', pData);
      if (isSuccessResponse(json)) {
        replaceContentWithSuccessMessage(id, opts);
        if (!!opts.onSuccessfulSubmit && typeof opts.onSuccessfulSubmit === 'function') {
          console.debug('calling on successful submit handler');
          try {
            opts.onSuccessfulSubmit({
              firstName: formData.firstName,
              lastName: formData.lastName,
              email: formData.adminEmail
            });
          } catch (e) {
            console.error("Problem calling the onSuccessfulSubmit handler!", e);
          }
        }
      } else {
        displayError(id, "Client error");
        console.warn("the post succeeded but we didn't receive the expected response");
      }
    } catch {
      displayError(id, "Server error");
      console.error("Could not post data");
    } finally {
      toggleFormDisabled(false);
      toggleFormSubmitting(id, false);
    }
  }
}

/**
 * toggles the form disabled state on and off with true and false respectively.
 * @param disabled if true, will disable the form, if false, will re-enable it.
 */
const toggleFormDisabled = (disabled: boolean) => { // no id because the other form should disable as well.
  const inputs: NodeListOf<HTMLInputElement> = document.querySelectorAll('.cs-signup-widget-container .input-section input');
  for (const input of inputs) {
    input.readOnly = disabled;
    disabled ? input.classList.add('read-only') : input.classList.remove('read-only');
  }
  const button: HTMLButtonElement = document.querySelector('.cs-signup-widget-container .submit-button');
  if (!!button) {
    button.disabled = disabled;
  }
}

const toggleFormSubmitting = (id: string, enabled: boolean) => {
  let container: Element = document.querySelector(`#${id}`);
  if (enabled) {
    container.classList.add('submitting');
  } else {
    container.classList.remove('submitting');
  }
}

/**
 * creates a ripple effect on the button (currentTarget) specified in the event object.
 * @param event
 */
const createRipple = (event: any) => {
  const button: any = event.currentTarget;
  const circle = document.createElement("span");
  const diameter = Math.max(button.clientWidth, button.clientHeight);
  const radius = diameter / 2;
  circle.style.width = circle.style.height = `${diameter}px`;
  circle.style.left = `${event.clientX - (button.offsetLeft + radius)}px`;
  circle.style.top = `${event.clientY - (button.offsetTop + radius)}px`;
  circle.classList.add("ripple");
  const ripple = button.getElementsByClassName("ripple")[0];
  if (ripple) {
    ripple.remove();
  }
  button.appendChild(circle);
}

const addTouched = (event: any) => {
  const input = event.currentTarget
  input.classList.add('touched');
}

const checkValidEmail = (event: any) => {
  // timeout so paste can operate correctly.
  let _event = event;
  setTimeout(() => {
    let input = _event.target as HTMLInputElement;
    if (!verifyValidEmail(input.value)) {
      input.setCustomValidity("Please enter a valid email address");
    } else {
      input.setCustomValidity("");
    }
  }, 0);
}

const replaceContentWithSuccessMessage = (id: string, opts: SignupOptions) => {
  let contentDiv = document.querySelector(`#${id} .content`);
  const successTemplate = require("./success.handlebars");
  contentDiv.innerHTML = successTemplate(opts).trim();
};

/**
 * Retrieves input data from a form and returns it as a JSON object.
 * @param  {HTMLFormControlsCollection} elements  the form elements
 * @return {Object}                               form data as an object literal
 */
const formToJSON = (elements: any[]): { [key: string]: string } => {
  return elements.reduceRight((data: { [key: string]: string }, element) => {
    data[element.name] = element.value;
    return data;
  }, {});
};

/**
 * Main export function
 * @param selector
 * @param opts
 */
const createSignupWidgetAsChildOf = (selector: string, opts: SignupOptions) => {
  const element = document.createElement('div');

  let id = `id_${makeId(20)}`;

  element.innerHTML = template({...opts, id: id}).trim();

  applyHandlers(element, id, opts);

  document.querySelectorAll(selector).forEach(v => {
    v.appendChild(element);
  });
};

export default createSignupWidgetAsChildOf;
