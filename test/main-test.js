/* global require */

require(['signupwidget'], function(widgetModule) {
  console.log(widgetModule);
  const createWidget = widgetModule.default;
  createWidget('.my-container', {title: 'Order Your Personal Social Check Now', buttonText: 'Order Now For Only $49', supportEmail: 'foobar@checksocial.com.au'});
});
