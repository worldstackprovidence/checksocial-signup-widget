require(['dist/signupwidget'], function(widgetModule) {
  const createWidget = widgetModule.default;
  createWidget('.my-container', {title: 'Order Your Personal Social Check Now', buttonText: 'Sign Up'});
  createWidget('.my-other-container', {title: '', buttonText: ''});
});
