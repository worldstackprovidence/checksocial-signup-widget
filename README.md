# Signup Widget

## Description

This widget will serve as a component that may be injected into a webpage for registering a person for a check in the checksocial application.

## building distributable

```shell script
npm install
npx webpack
```
