## Checksocial Signup widget

The bundle is packaged in UMD format and is assigned to a global variable named `cssignupwidget`.

## Installing the bundle

Reference the script as you normally would using a script tag, then add your own code beneath to reference the
global variable `cssignupwidget`

```html
<script async src="libs/signupwidget.js"></script>
<script src="dist/signupwidget.js"></script>
<script>
console.log(cssignupwidget);
const createWidget = cssignupwidget.default;
createWidget('.my-container', {title: 'The Title', buttonText: 'click me!'});
</script>
```

if you want to name the module something else then simply edit the bundle file, the assignment is the first word in that file.

You may customize the options provided in the second argument

    title: the heading title above the form
    buttonText: the white text that appears on the button
    supportEmail: email address to send problems to
    onSuccessfulSubmit: function with the shape: `(data: {firstName: string, lastName: string, email: string}) => any` that will be called on succesful submission of the form.
                        e.g. `function(data) => { console.log('user successfully submitted with email: "' + data.email + '"'); }`
    
